function alert_r(data){ alert(JSON.stringify(data,null,'    ')); }

var ranks = {};
var translations = {};
var experience = {};
var stype = {};

var tmpRecord = {};
var snd = new Audio();

/* LOCAL GAME DATA
--------------------------------------*/
var master = {
	ship:[],
	slotitem:[],
	useitem:[],
	furniture:[],
	payitem:[],
};

var userData = {
	hq:{
		name:"",
		rank:"",
		lv:"",
		exp:"",
		next:""
	},
	count:{
		ships:"",
		max_ships:"",
		gears:"",
		max_gears:""
	},
	reso:{
		fuel: "?",
		ammo: "?",
		steel: "?",
		bauxite: "?",
		irepair: "?",
		ibuild: "?"
	},
	fleets:[[],[],[],[]],
	ships:[],
	gears:[]
};

var quests = {
	
};

var timers = {

};


/* TRANSLATE WORD
--------------------------------------*/
function translate(jp_name){
	var my_english = jp_name;
	var last1 = jp_name.substr(jp_name.length-1, 1);
	var bare1 = jp_name.substr(0, jp_name.length-1);
	var last2 = jp_name.substr(jp_name.length-2, 2);
	var bare2 = jp_name.substr(0, jp_name.length-2);
	
	if(typeof translations[jp_name] !=="undefined"){
		my_english = translations[jp_name];
	}else{
		if(last1=="改"){
			if(typeof translations[bare1] !=="undefined"){
				my_english = translations[bare1]+" Kai";
			}
		}else if(last1=="*"){
			if(typeof translations[bare1] !=="undefined"){
				my_english = translations[bare1]+"*";
			}
		}else if(last2=="改二"){
			if(typeof translations[bare2] !=="undefined"){
				my_english = translations[bare2]+" Kai2";
			}
		}
	}
	
	return my_english;
}

/* UTILITIES
--------------------------------------*/
function pad(n, width, z) {
	z = z || '0'; n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function findParam(param, name){
	for(pctr in param){
		if(param[pctr].name == name){
			return param[pctr].value;
		}
	}
}


/* TIMERS - Countdown only
--------------------------------------*/
var timers = [[],[],[]];
var voiceNums = [7,6,5];

function stopTimer(element, type, num){
	timers[type][num].stop();
	$(element+" .time").text("");
	chrome.runtime.sendMessage({
		game:"kancolle",
		type:"background",
		action:"stop_timer",
		timer:{
			type:type,
			num:num
		}
	}, function(response) {});
}

function applyTimer(element, type, num, seconds){
	if(seconds>0){
		if(typeof timers[type][num] != "undefined"){ timers[type][num].stop(); }
		timers[type][num] = {
			element: element,
			seconds: seconds,
			type: type,
			t_hr: 0,
			t_mn: 0,
			t_sc: 0,
			interval:{},
			start: function(){
				this.interval = setInterval(
					(function(self){ return function(){ self.tick(); }})(this)
				, 1000);
			},
			tick: function(){
				this.seconds--;
				if(this.seconds>0){
					this.t_sc = this.seconds;
					this.t_hr = Math.floor(this.t_sc/3600); this.t_sc -= this.t_hr * 3600;
					this.t_mn = Math.floor(this.t_sc / 60) % 60; this.t_sc -= this.t_mn * 60;
					this.t_sc = Math.ceil(this.t_sc);
					$(this.element+" .time").text( pad(this.t_hr,2) +":"+ pad(this.t_mn,2) +":"+ pad(this.t_sc,2) );
				}else{
					switch(this.type){
						case 0: $(this.element+" .time").text("Complete!"); break;
						case 1: $(this.element+" .time").text(""); break;
						case 2: $(this.element+" .time").text("Complete!"); break;
						default: $(this.element+" .time").text(""); break;
					}
					this.stop();
				}
			},
			stop:function(){
				clearInterval(this.interval);
			}
		};
		timers[type][num].start();
		
		chrome.runtime.sendMessage({
			game:"kancolle",
			type:"background",
			action:"start_timer",
			timer:{
				type:type,
				num:num,
				seconds:seconds
			}
		}, function(response) {});
	}else{
		if(type==1){
			$(element+" .time").text("");
		}else{
			$(element+" .time").text("Complete!");
		}
	}
}


$(document).ready(function(){
	
	/* HTML ACTIONS
	--------------------------------------*/
	$("#loadNotice .start_btn").on('click', function(){
		chrome.runtime.sendMessage({
			game:"kancolle",
			type:"game",
			action:"activate"
		}, function(response){
			if(response.success){
				$("#loadNotice").hide();
				$("#wrapper").show();
			}else{
				
			}
		});
	});
	
	$(".navItem").on('click', function(){
		$(".tab-content").hide();
		$("#tab-"+$(this).data("box")).show();
		$(".navItem").removeClass("navActive");
		$(this).addClass("navActive");
		
		switch($(this).data("box")){
			case "ships":
				POPULATE_SHIPS();
				break;
			case "items":
				POPULATE_ITEMS();
				break;
			default: break;
		}
	});
	
	$("#navhome").click();
	
	$(".api_link input").val(localStorage["absoluteswf"]);
	$(".api_link").on('click', function(){
		$(".api_link input").select();
	});
	
	$(".api_revoke button").on('click', function(){
		chrome.runtime.sendMessage({
			game:"kancolle",
			type:"game",
			action:"forget"
		}, function(response){
			$("#wrapper").fadeOut();
		});
	});
	
	$(".sstool").on('click', function(){
		$(".sstool").hide();
		chrome.runtime.sendMessage({
			game:"kancolle",
			type:"game",
			action:"screenshot"
		}, function(response){
			$(".sstool").show();
		});
	});
	
	$("#ssmode_"+localStorage['ssmode']).prop('checked', true);
	$("input[name=ssmode]").on('change', function(){
		if($(this).prop("checked")){
			localStorage['ssmode'] = $(this).val();
		}
	});
	
	$("#asdasd").on('click', function(){
		alert(localStorage['ssmode']);
	});
	
	/* CHECK BG FOR EXISTING DATA
	--------------------------------------*/
	chrome.runtime.sendMessage({
		game:"kancolle",
		type:"background",
		action:"get_masters"
	}, function(response) {
		if(typeof response.master.ship != "undefined"){
			master = response.master;
			$("#loadNotice").hide();
			$("#wrapper").show();
		}
	});
	
	chrome.runtime.sendMessage({
		game:"kancolle",
		type:"background",
		action:"get_gears"
	}, function(response){
		if(response.gears.length > 0){
			userData.gears = response.gears;
		}
	});
	
	
	/* LOAD INITIAL DATA
	--------------------------------------*/
	$.getJSON("../json/ranks.json",function(response){ ranks=response; });
	$.getJSON("../json/translations.json",function(response){ translations=response; });
	$.getJSON("../json/experience.json",function(response){ experience=response; });
	$.getJSON("../json/stype.json",function(response){ stype=response; });

	/* LISTEN TO CHROME NETWORK
	--------------------------------------*/
	chrome.devtools.network.onRequestFinished.addListener(function(request) {
		var indexOfkcs = request.request.url.indexOf("/kcsapi/");
		if(indexOfkcs>-1){
			var thisCall = request.request.url.substring(indexOfkcs+8);
			request.getContent(function(requestContent){
				if(requestContent.indexOf("svdata=")>-1){ requestContent = requestContent.substring(7); }
				receiveData(thisCall, request.request.postData.params, JSON.parse(requestContent));
			});
		}
	});
	
	/* RECEIVE DATA FROM CHROME
	--------------------------------------*/
	function receiveData(api_call, params, response){
		chrome.runtime.sendMessage({
			game:"kancolle",
			type:"game",
			action:"clear_overlays"
		}, function(response){});
		switch(api_call){
			case "api_start2":
				for(i in response.api_data.api_mst_ship){
					tmpRecord = response.api_data.api_mst_ship[i];
					if(typeof tmpRecord.api_name != "undefined" && tmpRecord.api_name != "なし"){
						master.ship[tmpRecord.api_id] = tmpRecord;
						master.ship[tmpRecord.api_id].english = translate(tmpRecord.api_name);
					}
				}
				for(i in response.api_data.api_mst_slotitem){
					tmpRecord = response.api_data.api_mst_slotitem[i];
					master.slotitem[tmpRecord.api_id] = tmpRecord;
				}
				chrome.runtime.sendMessage({
					game:"kancolle",
					type:"background",
					action:"set_masters",
					master: master
				}, function(response){});
				$("#loadNotice").hide();
				$("#wrapper").show();
				break;
			
			case "api_port/port":
				userData.hq.name = response.api_data.api_basic.api_nickname;
				userData.hq.rank = ranks[response.api_data.api_basic.api_rank];
				userData.hq.lv = response.api_data.api_basic.api_level;
				userData.hq.exp = response.api_data.api_basic.api_experience;
				userData.hq.next = experience[userData.hq.lv+1][1] - userData.hq.exp;
				
				userData.ships = [];
				for(i in response.api_data.api_ship){
					userData.ships[response.api_data.api_ship[i].api_id] = response.api_data.api_ship[i];
				}
				
				userData.count.ships = response.api_data.api_ship.length;
				userData.reso = {
					fuel: response.api_data.api_material[0].api_value,
					ammo: response.api_data.api_material[1].api_value,
					steel: response.api_data.api_material[2].api_value,
					bauxite: response.api_data.api_material[3].api_value,
					irepair: response.api_data.api_material[5].api_value,
					ibuild: response.api_data.api_material[4].api_value
				};
				userData.fleets[0] = response.api_data.api_deck_port[0].api_ship;
				if(response.api_data.api_deck_port.length>1){
					userData.fleets[1] = response.api_data.api_deck_port[1].api_ship;
				}
				if(response.api_data.api_deck_port.length>2){
					userData.fleets[2] = response.api_data.api_deck_port[2].api_ship;
				}
				if(response.api_data.api_deck_port.length>3){
					userData.fleets[3] = response.api_data.api_deck_port[3].api_ship;
				}
				
				UPDATE_TIMERS_EXPED(response.api_data.api_deck_port);
				UPDATE_TIMERS_REPAIR(response.api_data.api_ndock);
				UPDATE_HEADER();
				UPDATE_MAINFLEET();
				break;
				
			case "api_get_member/record":
				userData.hq.name = response.api_data.api_nickname;
				userData.hq.rank = ranks[response.api_data.api_rank];
				userData.hq.lv = response.api_data.api_level;
				userData.hq.exp = response.api_data.api_experience[0];
				userData.hq.next = response.api_data.api_experience[1] - userData.hq.exp;
				userData.count.ships = response.api_data.api_ship[0];
				userData.count.max_ships = response.api_data.api_ship[1];
				userData.count.gears = response.api_data.api_slotitem[0];
				userData.count.max_gears = response.api_data.api_slotitem[1];
				UPDATE_HEADER();
				break;
				
			case "api_req_kousyou/getship":
				$("#timer_build_"+findParam(params, "api%5Fkdock%5Fid")+" .type img").attr("src","../img/ui/mo_W.png");
				$("#timer_build_"+findParam(params, "api%5Fkdock%5Fid")+" .time").text("");
				userData.count.ships += 1;
				UPDATE_HEADER();
				break;
				
			case "api_req_kousyou/createship_speedchange":
				$("#timer_build_"+findParam(params, "api%5Fkdock%5Fid")+" .type img").attr("src","../img/ui/mo_W.png");
				$("#timer_build_"+findParam(params, "api%5Fkdock%5Fid")+" .time").text("");
				stopTimer("#timer_build_"+findParam(params, "api%5Fkdock%5Fid"), 2, findParam(params, "api%5Fkdock%5Fid")-1);
				break;
				
			case "api_req_nyukyo/speedchange":
				$("#timer_repair_"+findParam(params, "api%5Fndock%5Fid")+" .type img").attr("src","../img/ui/mo_W.png");
				$("#timer_repair_"+findParam(params, "api%5Fndock%5Fid")+" .time").text("");
				stopTimer("#timer_repair_"+findParam(params, "api%5Fndock%5Fid"), 1, findParam(params, "api%5Fndock%5Fid")-1);
				break;
				
			case "api_req_kousyou/destroyitem2":
				var numScrap = params[0].value.split(",")
				userData.count.gears -= numScrap.length;
				UPDATE_HEADER();
				break;
				
			case "api_req_kousyou/destroyship":
				userData.count.ships -= 1;
				UPDATE_HEADER();
				break;
				
			case "api_req_kousyou/createitem":
				if(response.api_data.api_create_flag==1){
					userData.count.gears += 1;
					UPDATE_HEADER();
				}
				break;
				
			case "api_get_member/ship2":
				for(i in response.api_data){
					userData.ships[response.api_data[i].api_id] = response.api_data[i];
				}
				for(i in response.api_data_deck){
					userData.fleets[i] = response.api_data_deck[i].api_ship;
				}
				UPDATE_MAINFLEET();
				break;
				
			case "api_get_member/slot_item":
				var gear_count = 0;
				for(i in response.api_data){
					userData.gears[response.api_data[i].api_id] = response.api_data[i].api_slotitem_id;
				}
				userData.count.gears = response.api_data.length;
				chrome.runtime.sendMessage({
					game:"kancolle",
					type:"background",
					action:"set_gears",
					gears: userData.gears
				}, function(response){});
				UPDATE_HEADER();
				break;
				
			case "api_get_member/ndock":
				UPDATE_TIMERS_REPAIR(response.api_data);
				break;
				
			case "api_get_member/kdock":
				UPDATE_TIMERS_BUILD(response.api_data);
				break;
			
			case "api_get_member/deck":
				UPDATE_TIMERS_EXPED(response.api_data);
				break;
				
			case "api_get_member/questlist":
				chrome.runtime.sendMessage({
					game:"kancolle",
					type:"game",
					action:"quest_overlay",
					questlist: response.api_data.api_list
				}, function(response){});
				UPDATE_QUESTS(response.api_data.api_list);
				break;
				
			
			default:
				break;
		}
	}
	
	/* UPDATE UI WITH INFO
	--------------------------------------*/
	function UPDATE_QUESTS(){
		
	}
	
	function UPDATE_TIMERS_EXPED(deck_port){
		var now = new Date().getTime();
		
		if(typeof deck_port[1] != "undefined"){
			if(deck_port[1].api_mission[0]==1){
				$("#timer_exped_1 .type").text("#"+deck_port[1].api_mission[1]);
				applyTimer("#timer_exped_1",0,1, ((deck_port[1].api_mission[2]-60000)-now)/1000);
			}else if(deck_port[1].api_mission[0]==2){
				$("#timer_exped_1 .type").text("#"+deck_port[1].api_mission[1]);
				$("#timer_exped_1 .time").text("Complete!");
			}else{
				$("#timer_exped_1 .type").text("");
				$("#timer_exped_1 .time").text("Idle...");
			}
		}
		
		if(typeof deck_port[2] != "undefined"){
			if(deck_port[2].api_mission[0]==1){
				$("#timer_exped_2 .type").text("#"+deck_port[2].api_mission[1]);
				applyTimer("#timer_exped_2",0,2, ((deck_port[2].api_mission[2]-60000)-now)/1000);
			}else if(deck_port[2].api_mission[0]==2){
				$("#timer_exped_2 .type").text("#"+deck_port[2].api_mission[1]);
				$("#timer_exped_2 .time").text("Complete!");
			}else{
				$("#timer_exped_2 .type").text("");
				$("#timer_exped_2 .time").text("Idle...");
			}
		}
		
		if(typeof deck_port[3] != "undefined"){
			if(deck_port[3].api_mission[0]==1){
				$("#timer_exped_3 .type").text("#"+deck_port[3].api_mission[1]);
				applyTimer("#timer_exped_3",0,3, ((deck_port[3].api_mission[2]-60000)-now)/1000);
			}else if(deck_port[3].api_mission[0]==2){
				$("#timer_exped_3 .type").text("#"+deck_port[3].api_mission[1]);
				$("#timer_exped_3 .time").text("Complete!");
			}else{
				$("#timer_exped_3 .type").text("");
				$("#timer_exped_3 .time").text("Idle...");
			}
		}
		
	}
	
	function UPDATE_TIMERS_REPAIR(dock_slots){
		if(dock_slots[2].api_state==-1){ $("#timer_repair_3").hide(); }
		if(dock_slots[3].api_state==-1){ $("#timer_repair_4").hide(); }
		var now = new Date().getTime();
		
		if(dock_slots[0].api_state==1){
			Repaired_ShipData = userData.ships[dock_slots[0].api_ship_id];
			$("#timer_repair_1 .type img").attr("src","../img/ships/"+Repaired_ShipData.api_ship_id+".jpg");
			applyTimer("#timer_repair_1",1,0, ((dock_slots[0].api_complete_time-60000)-now)/1000);
		}else if(dock_slots[0].api_state==0){
			$("#timer_repair_1 .type img").attr("src","../img/ui/mo_W.png");
			$("#timer_repair_1 .time").text("");
		}
		
		if(dock_slots[1].api_state==1){
			Repaired_ShipData = userData.ships[dock_slots[1].api_ship_id];
			$("#timer_repair_2 .type img").attr("src","../img/ships/"+Repaired_ShipData.api_ship_id+".jpg");
			applyTimer("#timer_repair_2",1,1, ((dock_slots[1].api_complete_time-60000)-now)/1000);
		}else if(dock_slots[1].api_state==0){
			$("#timer_repair_2 .type img").attr("src","../img/ui/mo_W.png");
			$("#timer_repair_2 .time").text("");
		}
		
		if(dock_slots[2].api_state==1){
			Repaired_ShipData = userData.ships[dock_slots[2].api_ship_id];
			$("#timer_repair_3 .type img").attr("src","../img/ships/"+Repaired_ShipData.api_ship_id+".jpg");
			applyTimer("#timer_repair_3",1,2, ((dock_slots[2].api_complete_time-60000)-now)/1000);
		}else if(dock_slots[2].api_state==0){
			$("#timer_repair_3 .type img").attr("src","../img/ui/mo_W.png");
			$("#timer_repair_3 .time").text("");
		}
		
		if(dock_slots[3].api_state==1){
			Repaired_ShipData = userData.ships[dock_slots[3].api_ship_id];
			$("#timer_repair_4 .type img").attr("src","../img/ships/"+Repaired_ShipData.api_ship_id+".jpg");
			applyTimer("#timer_repair_4",1,3, ((dock_slots[3].api_complete_time-60000)-now)/1000);
		}else if(dock_slots[3].api_state==0){
			$("#timer_repair_4 .type img").attr("src","../img/ui/mo_W.png");
			$("#timer_repair_4 .time").text("");
		}
	}
	
	function UPDATE_TIMERS_BUILD(dock_slots){
		if(dock_slots[2].api_state==-1){ $("#timer_build_3").hide(); }
		if(dock_slots[3].api_state==-1){ $("#timer_build_4").hide(); }
		var now = new Date().getTime();
		if(dock_slots[0].api_created_ship_id>0){
			$("#timer_build_1 .type img").attr("src","../img/ships/"+dock_slots[0].api_created_ship_id+".jpg");
			applyTimer("#timer_build_1",2,0, (dock_slots[0].api_complete_time-now)/1000);
		}
		if(dock_slots[1].api_created_ship_id>0){
			$("#timer_build_2 .type img").attr("src","../img/ships/"+dock_slots[1].api_created_ship_id+".jpg");
			applyTimer("#timer_build_2",2,1, (dock_slots[1].api_complete_time-now)/1000);
		}
		if(dock_slots[2].api_created_ship_id>0){
			$("#timer_build_3 .type img").attr("src","../img/ships/"+dock_slots[2].api_created_ship_id+".jpg");
			applyTimer("#timer_build_3",2,2, (dock_slots[2].api_complete_time-now)/1000);
		}
		if(dock_slots[3].api_created_ship_id>0){
			$("#timer_build_4 .type img").attr("src","../img/ships/"+dock_slots[3].api_created_ship_id+".jpg");
			applyTimer("#timer_build_4",2,3, (dock_slots[3].api_complete_time-now)/1000);
		}
	}
	
	function UPDATE_HEADER(){
		$("#hq-name").text(userData.hq.name);
		$("#hq-rank").text(userData.hq.rank);
		$("#hq-lv").text("HQ Lv."+userData.hq.lv);
		$("#hq-next").text("Next: "+userData.hq.next+" exp");
		$("#count-irepair").text(userData.reso.irepair);
		$("#count-ibuild").text(userData.reso.ibuild);
		$("#count-ship").text(userData.count.ships);
		$("#count-maxship").text("/ "+userData.count.max_ships);
		$("#count-gear").text(userData.count.gears);
		$("#count-maxgear").text("/ "+userData.count.max_gears);
	}
	
	function UPDATE_MAINFLEET(){
		$("#main_fleet").html("");
		
		for(i in userData.fleets[0]){
			if(userData.fleets[0][i] >-1){
				My_ShipID = userData.fleets[0][i];
				My_ShipData = userData.ships[My_ShipID];
				Ms_ShipID = My_ShipData.api_ship_id;
				Ms_ShipData = master.ship[Ms_ShipID];
				
				tmpShipBox = $("#factory .ship").clone().appendTo("#main_fleet");
				$(".pic img", tmpShipBox).attr("src", "../img/ships/"+Ms_ShipID+".jpg");
				$(".en_name", tmpShipBox).text(translate(Ms_ShipData.api_name));
				$(".stype", tmpShipBox).text(stype[Ms_ShipData.api_stype]);
				$(".jp_name", tmpShipBox).text(Ms_ShipData.api_name);
				
				$(".life .hp strong", tmpShipBox).text(My_ShipData.api_nowhp);
				$(".life .hp span", tmpShipBox).text("/"+My_ShipData.api_maxhp);
				
				$("body").css("background", "#ddd");
				
				tmpBarPercent = My_ShipData.api_nowhp/My_ShipData.api_maxhp;
				$(".life .bar_val", tmpShipBox).css("width", (60*tmpBarPercent)+"px");
				if(tmpBarPercent<=0.2){
					$(".life .bar_val", tmpShipBox).css("background", "#f00");
					$("body").css("background", "#fcc");
				}else if(tmpBarPercent<=0.5){
					$(".life .bar_val", tmpShipBox).css("background", "#F99500");
				}else if(tmpBarPercent<=0.8){
					$(".life .bar_val", tmpShipBox).css("background", "#ffcc00");
				}else{
					$(".life .bar_val", tmpShipBox).css("background", "#0f0");
				}
				
				$(".morale", tmpShipBox).text(My_ShipData.api_cond);
				if(My_ShipData.api_cond>49){
					$(".morale", tmpShipBox).css("background", "url(../img/ui/mo_Y.png)");
				}else if(My_ShipData.api_cond>29){
					$(".morale", tmpShipBox).css("background", "url(../img/ui/mo_W.png)");
				}else if(My_ShipData.api_cond>19){
					$(".morale", tmpShipBox).css("background", "url(../img/ui/mo_O.png)");
				}else{
					$(".morale", tmpShipBox).css("background", "url(../img/ui/mo_R.png)");
				}
				
				$(".leveltxt", tmpShipBox).html("<span>Lv.</span> <strong>"+My_ShipData.api_lv+"</strong>");
				tmpBarWidth = Math.floor((My_ShipData.api_exp[2]/100)*60);
				var expLeft = My_ShipData.api_exp[1];
				$(".level .bar_val", tmpShipBox).css("width", tmpBarWidth+"px");
				$(".level .bar_val", tmpShipBox).css("background", "#00CCFF");
				$(".level .bar_txt", tmpShipBox).text("-"+expLeft);
			}
		}
		
	}
	
	function POPULATE_SHIPS(){
		$("#tab-ships").html("");
		var tmpBox;
		for(i in userData.ships){
			My_Ship = userData.ships[i];
			Ms_ShipID = My_Ship.api_ship_id;
			Ms_ShipData = master.ship[Ms_ShipID];
			
			tmpBox = $("#factory .shipli").clone().appendTo("#tab-ships");
			$(".spic img", tmpBox).attr("src", "../img/ships/"+Ms_ShipID+".jpg");
			$(".level", tmpBox).html("Lv.<strong>"+My_Ship.api_lv+"</strong>");
			$(".name", tmpBox).text(Ms_ShipData.english);
			
			if(My_Ship.api_cond>49){
				$(".morale img", tmpBox).attr("src", "../img/ui/mo_Y.png");
			}else if(My_Ship.api_cond>29){
				$(".morale img", tmpBox).attr("src", "../img/ui/mo_W.png");
			}else if(My_Ship.api_cond>19){
				$(".morale img", tmpBox).attr("src", "../img/ui/mo_O.png");
			}else{
				$(".morale img", tmpBox).attr("src", "../img/ui/mo_R.png");
			}
			
			$(".stat.hp", tmpBox).text(My_Ship.api_maxhp);
			$(".stat.fp", tmpBox).text(My_Ship.api_karyoku[0]);
			$(".stat.tp", tmpBox).text(My_Ship.api_raisou[0]);
			$(".stat.aa", tmpBox).text(My_Ship.api_taiku[0]);
			$(".stat.ar", tmpBox).text(My_Ship.api_soukou[0]);
			$(".stat.as", tmpBox).text(My_Ship.api_taisen[0]);
			$(".stat.ev", tmpBox).text(My_Ship.api_kaihi[0]);
			$(".stat.ls", tmpBox).text(My_Ship.api_sakuteki[0]);
			$(".stat.lk", tmpBox).text(My_Ship.api_lucky[0]);
			
			$(".gear", tmpBox).html("");
			for(j in My_Ship.api_slot){
				My_ItemID = My_Ship.api_slot[j];
				if(My_ItemID>-1){
					Ms_ItemID = userData.gears[My_ItemID];
					Ms_ItemData = master.slotitem[Ms_ItemID];
					$(".gear", tmpBox).append('<img src="../img/items/'+Ms_ItemData.api_type[2]+'.png" />');
				}
			}
			
			if(My_Ship.api_locked==0){
				$(".lock img", tmpBox).hide();
			}
		}
	}
	
	function POPULATE_ITEMS(){
		$("#tab-items").html("");
		
	}
	
});

