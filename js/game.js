var waiting = false;
var quests = {};

$(document).ready(function(){
	$.getJSON("../json/quests.json",function(response){ quests=response; });
	
	var absoluteSwf = localStorage["absoluteswf"];
	if(absoluteSwf){
		$(".api_txt").text(localStorage["absoluteswf"])
		$("#wait-box").show();
		waiting = true;
	}else{
		$("#no-api").show();
	}
	
	$(".api_submit").on('click', function(){
		localStorage["absoluteswf"] = $(".api_text").val();
		window.location.reload();
	});
	
	$(".forget_btn").on('click', function(){
		localStorage["absoluteswf"] = "";
		window.location.reload();
	});
	
	$(".play_btn").on('click', function(){
		$("#wait-box").hide();
		$("#game-box").show();
		$("#game-swf").attr("src", localStorage["absoluteswf"]);
	});
	
});

var ssdownloader = document.createElement("a");
ssdownloader.setAttribute("download", "kancolle.jpg");
var imgur_img = "";
var sswaitresponse = {};


var canvas = document.createElement("canvas");
var context = canvas.getContext("2d");
canvas.width = 800;
canvas.height = 480;

function cropImage(base64img, callback){
	var ss_image = new Image();
	ss_image.onload = function() {
		context.drawImage(ss_image, $("#game-box").offset().left, $("#game-box").offset().top, 800, 480, 0, 0, 800, 480);
		callback(canvas.toDataURL("image/jpeg"));
	}
	ss_image.src = base64img;
}

chrome.runtime.onMessage.addListener(function(request, sender, response) {
    if(request.game==="kancolle" && request.type==="game"){
		switch(request.action){
			case "screenshot":
				console.log("Screenshot tool used.");
				chrome.tabs.captureVisibleTab(null, {format:"jpeg"}, function(base64img){
					cropImage(base64img, function(base64img){
						switch(localStorage['ssmode']){
							case "tab":
								window.open(base64img, "_blank");
								break;
							case "download":
								ssdownloader.href = base64img;
								ssdownloader.click();
								break;
							case "imgur":
								$.ajax({
									url: 'https://api.imgur.com/3/credits',
									method: 'GET',
									headers: {
										Authorization: 'Client-ID 088cfe6034340b1',
										Accept: 'application/json'
									},
									success: function(response){
										if(response.data.UserRemaining>10 && response.data.ClientRemaining>100){
											$.ajax({
												url: 'https://api.imgur.com/3/image',
												method: 'POST',
												headers: {
													Authorization: 'Client-ID 088cfe6034340b1',
													Accept: 'application/json'
												},
												data: {
													image: base64img.substring(23),
													type: 'base64'
												},
												success: function(response){
													window.open(response.data.link, "_blank");
												}
											});
										}else{
											alert("[ScreenShot Tool] Sorry, KC3 Player's IMGUR image upload limit has exceeded. This limit is shared throughout all players. You may upload ScreenShots again tomorrow. In the meantime, select new tab or download in the ScreenShot settings of KC3 Player.");
										}
									}
								});
								break;
							default: break;
						}
					});
				});
				response({success:true});
				break;
			case "quest_overlay":
				for(qci in request.questlist){
					if(request.questlist[qci]!=-1){
						var tmpQuestOverlay = $("#factory .ol_quest").clone().appendTo("#overlays");
						tmpQuestOverlay.css("top", (113+(qci*68))+"px");
						if(typeof quests[request.questlist[qci].api_no] != "undefined"){
							$(".name", tmpQuestOverlay).text(quests[request.questlist[qci].api_no].name);
							$(".desc", tmpQuestOverlay).text(quests[request.questlist[qci].api_no].desc);
						}else{
							$(".name", tmpQuestOverlay).text("[No translation] "+request.questlist[qci].api_title);
							$(".desc", tmpQuestOverlay).text(request.questlist[qci].api_detail);
							// send report
						}
					}
				}
				break;
			case "clear_overlays":
				$("#overlays").html("");
				break;
			case "activate":
				if(waiting){
					$("#wait-box").hide();
					$("#game-box").show();
					$("#game-swf").attr("src", localStorage["absoluteswf"]);
					response({success:true});
				}else{
					response({success:false});
				}
				break;
			case "forget":
				localStorage["absoluteswf"] = "";
				window.location.reload();
				response({success:true});
				break;
			default:
				response({success:false, message:"Unknown action"});
				break;
		}
	}
});