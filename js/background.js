/* MASTER DATA for later use
--------------------------------------*/
var master = {};
var user_gears = [];


/* APP SETTINGS
--------------------------------------*/
var default_options = {
	cookiehack: "true",
	autodmm: "true",
	absoluteswf: "",
	ssmode: "tab",
};

for(i in default_options){
	if(!localStorage[i]){
		localStorage[i] = default_options[i];
	}
}

function resetToDefaults(){
	localStorage.clear();
	for(i in default_options){
		localStorage[i] = default_options[i];
	}
}

/* TIMERS - Notification
--------------------------------------*/
var notifMsg = [
	{ title:"Expedition Complete!", desc:"One of your fleets has returned from their expedition.", icon:"_expedition" },
	{ title:"Docking Complete!", desc:"One of your ships has been fully repaired.", icon:"_supply" },
	{ title:"Construction Complete!", desc:"A ship construction has been successfully completed.", icon:"_build" }
];

var timers = [[],[],[]];
function start_timer(type, num, seconds){
	if(typeof timers[type][num] != "undefined"){ timers[type][num].stop(); }
	
	timers[type][num] = {
		notifId: "kc3t_"+type+"_"+num,
		notifTitle: notifMsg[type].title,
		notifDesc: notifMsg[type].desc,
		notifIcon: notifMsg[type].icon,
		
		interval:{},
		
		seconds: seconds,
		t_hr: 0,
		t_mn: 0,
		t_sc: 0,
		
		start: function(){
			chrome.notifications.clear(this.notifId, function(){});
			this.interval = setInterval(
				(function(self){ return function(){ self.tick(); }})(this)
			, 1000);
		},
		tick: function(){
			this.seconds--;
			if(this.seconds<=0){
				snd = new Audio("../aud/bell.mp3"); snd.play();
				chrome.notifications.create(this.notifId, {
					type: "basic",
					title: this.notifTitle,
					message: this.notifDesc,
					iconUrl: "../img/quests/"+this.notifIcon+".jpg"
				}, function(){});
				this.stop();
			}
		},
		stop:function(){
			clearInterval(this.interval);
		}
	};
	
	timers[type][num].start();
}


/* EXTENSION INTERACTION
--------------------------------------*/
chrome.runtime.onMessage.addListener(function(request, sender, response) {
    if(request.game==="kancolle" && request.type==="background"){
		switch(request.action){
			case "start_timer":
				start_timer(request.timer.type, request.timer.num, request.timer.seconds);
				console.log("Timer started");
				response({success:true});
				break;
			case "stop_timer":
				timers[request.timer.type][request.timer.num].stop();
				console.log("Timer stopped");
				response({success:true});
				break;
			case "set_gears":
				user_gears = request.gears;
				console.log("User slotitems received.");
				response({success:true});
				break;
			case "get_gears":
				console.log("Responding with User slotitems.");
				response({success:true, gears:user_gears});
				break;
			case "set_masters":
				master = request.master;
				console.log("Master Data received.");
				response({success:true});
				break;
			case "get_masters":
				console.log("Responding with Master Data.");
				response({success:true, master:master});
				break;
			case "reset_defaults":
				resetToDefaults();
				console.log("Default Settings restored.");
				response({success:true});
				break;
			case "set_option":
				localStorage[request.field] = request.value;
				console.log("["+request.field+"] received and stored on localStorage.");
				response({success:true});
				break;
			case "get_option":
				if(!localStorage[request.field]){
					localStorage[request.field] = default_options[request.field];
				}
				console.log("Responding with local-["+request.field+"]");
				response({success:true, value:localStorage[request.field]});
				break;
			case "get_default":
				console.log("Responding with ["+request.field+"] settings.");
				response({success:true, value:default_options[request.field]});
				break;
			case "setup_game":
				localStorage["absoluteswf"] = request.swfsrc;
				window.open("html/game.html", "kc3_game");
				console.log("Game API Link received. Opening game.");
				response({success:true});
				break;
			default:
				response({success:false, message:"Unknown action"});
				break;
		}
	}
});